package commonmethodpackage;
import static io.restassured.RestAssured.given;

import request_respositorypackage.Endpoints;

public class Trigger_API_GetMethod extends Endpoints{
	
	public static int extract_status_code(String URL) {
		int Statuscode = given().when().get(URL).then().extract().statusCode();
		return Statuscode;
	}
	public static String extract_response_body(String URL) {
		String ResponseBody = given().when().get(URL).then().extract().response().asString();
		return ResponseBody;
}

}
