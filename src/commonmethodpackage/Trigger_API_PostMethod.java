package commonmethodpackage;
import java.io.File;

import request_respositorypackage.post_request_repository;

import static io.restassured.RestAssured.given;

public class Trigger_API_PostMethod extends post_request_repository {

	// create method to extract Status code

	public static int extract_status_code(String req_Body, String postURL) {

		int Statuscode = given().header("Content-Type", "application/json").body(req_Body).when().post(postURL).then()
				.extract().statusCode();

		return Statuscode;
	}
	// create method to extract response body

	public static String extract_responsebody(String req_Body, String URL) {

		String responseBody = given().header("Content-Type", "application/json").body(req_Body).when().post(URL).then()
				.extract().response().asString();
		return responseBody;

	}

}
