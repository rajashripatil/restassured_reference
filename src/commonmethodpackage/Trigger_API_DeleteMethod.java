package commonmethodpackage;

import static io.restassured.RestAssured.given;

import request_respositorypackage.Endpoints;


public class Trigger_API_DeleteMethod extends Endpoints {
	// create method to extract Status code

			public static int extract_status_code(String deleteURL) {

				int Statuscode = given().header("Content-Type", "application/json").when().delete(deleteURL).then()
						.extract().statusCode();

				return Statuscode;

}
}