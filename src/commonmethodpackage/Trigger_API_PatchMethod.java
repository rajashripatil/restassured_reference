package commonmethodpackage;

import static io.restassured.RestAssured.given;

import request_respositorypackage.Patch_request_repository;
//import request_respositorypackage.post_request_repository;

public class Trigger_API_PatchMethod extends Patch_request_repository {
	// create method to extract Status code

	public static int extract_status_code(String req_Body, String patchURL) {

		int Statuscode = given().header("Content-Type", "application/json").body(req_Body).when().patch(patchURL).then()
				.extract().statusCode();

		return Statuscode;
	}
	// create method to extract response body

	public static String extract_responsebody(String req_Body, String URL) {

		String responseBody = given().header("Content-Type", "application/json").body(req_Body).when().patch(URL).then()
				.extract().response().asString();
		return responseBody;

	}
}
