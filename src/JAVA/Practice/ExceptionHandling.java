package JAVA.Practice;

public class ExceptionHandling {

	/*public static void main(String[] args) {
		int a = 10, b = 0, c;
		c = a / b;
		System.out.println(c);
//Arithmetic Exception
	}
	
	*/
	public static void main(String[] args) {
		System.out.println("main method started");
		int a=10,b=0,c;
		try{
			c=a/b;
			System.out.println(c);
			}
		catch(Exception e) {
			System.out.println("Can't devide by zero");
			
		}
		System.out.println("Method ended");
	}
	
	
	
	
	
}
