package JAVA.Practice;

public class Strings {

	public static void main(String[] args) {

		String statement= "hello world i m learning java";
		//create an array to store word separator like space
	    String[] str= statement.split (" ");
	    //calculate no of words and store in variable 
		//int i = str.length;
		//System.out.println(i);
		
		//Find character at 4th place
		//System.out.println(statement.charAt(4));
		
		//equal ignore case : it compares two strings irrespective of upper lower cases
		//System.out.println(statement.equalsIgnoreCase("HELLO world i m learning java"));
		
		System.out.println(statement.indexOf("world")); 
		
	}

}
