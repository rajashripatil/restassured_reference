package JAVA.Practice;

import java.util.Scanner;

public class Reverse_number {

	public static void main(String[] args) {
		
      Scanner sc= new Scanner(System.in); //take input from user at runtime
      System.out.println("Enter the number: ");
      
      int num= sc.nextInt();  //accept value from user
      
	//use StringBuffer class
      
   
      
      StringBuffer sb= new StringBuffer(String.valueOf(num));//String.value of is method to convert number into string
      StringBuffer rev=sb.reverse(); //reverse(); is the method to reverse number
      
      System.out.println("Reverse Number is:  " +rev);
		
	}

}
