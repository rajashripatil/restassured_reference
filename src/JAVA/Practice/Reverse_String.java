package JAVA.Practice;

public class Reverse_String {

	public static void main(String[] args) {
		String Str = "Poorvaja";

		StringBuffer sb = new StringBuffer(Str);
		System.out.println(sb.reverse());
	}

}

//Using STringBuffer class

//String str = "Rajashri";
//StringBuffer sb = new StringBuffer(str);
//StringBuffer ReverseString = sb.reverse();
//System.out.println(ReverseString);
