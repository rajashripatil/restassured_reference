  package request_respositorypackage;

import java.io.IOException;
import java.util.ArrayList;

import commonutilitypackage.Excel_data_reader;

public class post_request_repository extends Endpoints{

	public static String post_tc1_request() throws IOException {
		
		ArrayList<String> exceldata = Excel_data_reader.Read_Excel_Data("Api_Data.xlsx", "Post_API", "Post_TC_2");
		System.out.println(exceldata);
		String req_name = exceldata.get(1);
		String req_job = exceldata.get(2);
		
		
	String  requestBody = "{\r\n"
			+ "    \"name\": \""+req_name+"\",\r\n"
			+ "    \"job\": \""+req_job+"\"\r\n"
			+ "}";
	return requestBody;
		}
	
	public static String put_tc1_request() {
		
		String  requestBody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		return requestBody;
	}
	public static String patch_tc1_request() {
		String  requestBody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		return requestBody;
	}
	
}
 
