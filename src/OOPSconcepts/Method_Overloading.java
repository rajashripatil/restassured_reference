
                                                      // Method Overloading Example


package OOPSconcepts;

public class Method_Overloading {
 
	
	public void add() {
		System.out.println("1");
	}
	
    public void add(int a, int b) {
		System.out.println("2");
	}
    
    public int add(int a, int b, int c) {
    	System.out.println("3");
    	return a;
		
	}
	//In Method Overloading Method name will be same but No of parameters will can be different 
    //and method return type can also be different
	
	
	
	
}
