
                                              //Method Overriding Example


package OOPSconcepts;

public class Scientific_Calculator_Override extends Calculator_Overriding_Concept {
	//override
	public void add() {

		System.out.println("Give the solution of complex problems: ");

	}

	public static void main(String[] args) {

		Scientific_Calculator_Override SC = new Scientific_Calculator_Override();
		SC.add();
	}
}

//Method overriding Method name will be same, data type and number of arguments will also be same only thing is here 
//subclass will override the method inherited from superclass and it will give its own implementation 