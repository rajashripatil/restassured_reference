                    //Abstraction using Abstract class

package OOPSconcepts;

abstract class Abstraction_Calculation {
         public abstract void calculator();
}

class add extends Abstraction_Calculation {

	public void calculator() {
		System.out.println("Calculates Addition of two numbers");
	}
}
 class sub extends  Abstraction_Calculation {
	public void calculator() {
		System.out.println("Calculates subtraction of two numbers");
	}
}
 class Main{
	public static void main (String[]args) {
		add a = new add();
		a.calculator();
		
		sub s = new sub();
		s.calculator();
	}
}