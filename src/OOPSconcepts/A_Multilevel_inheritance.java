package OOPSconcepts;


public class A_Multilevel_inheritance {
	public void add() {
		int a = 10;
		int b = 20;
		int c = a + b;
		System.out.println("Addition is : "+c);
	}

	public void sub() {
		int a = 1000;
		int b = 200;
		int c = a - b;
		System.out.println("Subtraction is : "+c);
	}
}

class B extends A_Multilevel_inheritance {
	public void multiply() {
		int a = 10;
		int b = 20;
		int c = a * b;
		System.out.println("Multiplication is : "+c);
	}

	public void division() {
		int a = 1000;
		int b = 200;
		int c = a / b;
		System.out.println("Division is : "+c);
	}

}

class c extends B {
	public void remain() {
		int a = 20;
		int b = 6;
		int c = a % b;
		System.out.println("Remainder is : "+c);
	}
}

class test {
	public static void main(String[] args) {
		c object = new c();
		object.add();
		object.multiply();
		object.sub();
		object.division();
		object.remain();
	}
}