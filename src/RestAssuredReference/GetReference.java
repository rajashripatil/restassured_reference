package RestAssuredReference;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;

public class GetReference {

	public static void main(String[] args) {
		//Step1 Declare the variable for BaseURI and Request body
				String BaseURI= "https://reqres.in/";

		        
		        //step 2: Declare Base URI
		        RestAssured.baseURI = BaseURI;
		        
		        //Step3 Configure Request body and Trigger API
		       
		    //  String responsebody= given().when().get("api/users/2")
		      //  .then().extract().response().asString();
		      // System.out.println(responsebody);
		        
		       given().log().all()
		        .when()
		        .get("api/users/2")
		        .then().log().all().extract().response().asString() ;
	}

}
