package RestAssuredReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Post_RequestBodyvalidation {

	public static void main(String[] args) {
		
		//Step1 Declare BaseURI and Request Body
		 String BaseURI= "https://reqres.in/";
		 String RequestBody= "{\r\n"
		 		+ "    \"name\": \"morpheus\",\r\n"
		 		+ "    \"job\": \"leader\"\r\n"
		 		+ "}"	; 
		 		
		//Step2 Declare BaseURI
		
		RestAssured.baseURI=BaseURI;
		
		//Step3 Configure Request body and trigger the API
		String ResponseBody=given().header("Content-Type", "application/json").body(RequestBody).
				when().post("api/users").
				then().extract().response().asString();
		
		//Step4 Create an object of Jsonpath to parse the Responsebody 
		
		JsonPath jsp_res= new JsonPath (ResponseBody);
		String res_name=jsp_res.getString("name");
		System.out.println(res_name);
		
		String res_job= jsp_res.getString("job");
		System.out.println(res_job);
		
		String res_id = jsp_res.getString("id");
		System.out.println(res_id);
		
		String res_createdAt = jsp_res.getString("createdAt");
		System.out.println(res_createdAt);
		
		//Step5 Create an object of Jsonpath to parse Request Body
		
		JsonPath jsp_req =new JsonPath (RequestBody);
		String req_name=jsp_req.getString("name");
		System.out.println(req_name);
		
		String req_job= jsp_req.getString("job");
		System.out.println(req_job);		
		
		//Step6 validate the response body parameter
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		
	
	}
	
}
