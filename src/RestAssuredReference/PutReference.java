package RestAssuredReference;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;

public class PutReference {

	public static void main(String[] args) {
		//Step1 Declare the variable for BaseURI and Request body
				String BaseURI = "https://reqres.in/";
		        String Requestbody = "{\r\n"
		        		+ "    \"name\": \"morpheus\",\r\n"
		        		+ "    \"job\": \"leader\"\r\n"
		        		+ "}";
		        
		        //step 2: Declare Base URI
		        RestAssured.baseURI = BaseURI;
		        
		        //Configure Request body and Trigger API
		       /*String responsebody= given().header("Content-Type","application/json").body(Requestbody).when().put("api/users/2")
		        .then().extract().response().asString();
		       System.out.println(responsebody);*/
		        
		        given().header("Content-Type","application/json").body(Requestbody).log().all()
		        .when()
		        .put("api/users/2")
		        .then().log().all().extract().response().asString() ;     
	}

}
