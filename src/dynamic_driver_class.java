import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import commonutilitypackage.Excel_data_reader;

public class dynamic_driver_class {

	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		// Step1 read the test cases to be executed from excel file

		ArrayList<String> TestCaseList = Excel_data_reader.Read_Excel_Data("Api_Data.xlsx", "TestCasesToExecute",
				"TestCaseToExecute");
		System.out.println(TestCaseList);
		int count = TestCaseList.size();

		for (int i = 1; i < count; i++) {

			String TestCaseToExecute = TestCaseList.get(i);
			System.out.println("Testcase which is to be executed is: " + TestCaseToExecute);

			// Step2 call the TestCaseToExecute on runtime by using java.lang.reflect package
			Class<?> TestClass = Class.forName("testclasspackage."+TestCaseToExecute);
			
			//Step3 Call the execute method of the class captured in variable in test class by using java.lang.reflect.methodc
			
		Method ExecuteMethod	=TestClass.getDeclaredMethod("executor");
		
		//step 4 set the accessibility of method as true
		
		ExecuteMethod.setAccessible(true);
		
		//Step 5 Create the instance of Class captured in class variable
		Object InstanceOfTestClass = TestClass.getDeclaredConstructor().newInstance();
		
		//Step 6 Execute the method captured in variable ExecuteMethod of class captured in  variable TestClass
		
		ExecuteMethod.invoke(InstanceOfTestClass);  
			
			
		}
	}

}
