package testclasspackage;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import commonmethodpackage.Trigger_API_GetMethod;
import commonutilitypackage.Handle_Get_logs;
import io.restassured.path.json.JsonPath;

public class Get_TC1 extends Trigger_API_GetMethod {
	@Test
	public static void executor() throws IOException {
		File DirectoryName = Handle_Get_logs.Create_log_Directory("Get_TC1");

		for (int i = 0; i < 5; i++) {
			int Statuscode = extract_status_code(get_endpoint());
			System.out.println("Status code: " + Statuscode);
			if (Statuscode == 200) {

				String ResponseBody = extract_response_body(get_endpoint());
				System.out.println("Response Body: " + ResponseBody);
				Handle_Get_logs.evidence_creator_Get(DirectoryName,"Get_TC1",get_endpoint(),ResponseBody);
				break;

			} else {
				System.out.println("Desired Sttaus code is not found hence,Retry");
			}
		}
	}

	public static void validator(String ResponseBody) {

		String exp_id_array[] = { "7", "8", "9", "10", "11", "12" };
		String exp_email_array[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String exp_first_name_array[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String exp_last_name_array[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String exp_avatar_array[] = { "https://reqres.in/img/faces/7-image.jpg",
				"https://reqres.in/img/faces/8-image.jpg", "https://reqres.in/img/faces/9-image.jpg",
				"https://reqres.in/img/faces/10-image.jpg", "https://reqres.in/img/faces/11-image.jpg",
				"https://reqres.in/img/faces/12-image.jpg" };

		JsonPath jsp_res = new JsonPath(ResponseBody);
		List<Object> res_data = jsp_res.getList("data");

		//to store list of array object from data(response body)...list is type of collection
		int count = res_data.size();
		String page = jsp_res.getString("page");
		String per_page = jsp_res.getString("per_page");
		String total = jsp_res.getString("total");
		String total_pages = jsp_res.getString("total_pages");

		Assert.assertEquals(page, "2");
		Assert.assertEquals(per_page, "6");
		Assert.assertEquals(total, "12");
		Assert.assertEquals(total_pages, "2");

		for (int i = 0; i < count; i++) {

			String Exp_id = exp_id_array[i];
			String res_id = jsp_res.getString("data[" + i + "].id");

			String Exp_email = exp_email_array[i];
			String res_email = jsp_res.getString("data.email" + "[" + i + "]");

			String Exp_first_name = exp_first_name_array[i];
			String res_first_name = jsp_res.getString("data.first_name" + "[" + i + "]");

			String Exp_last_name = exp_last_name_array[i];
			String res_last_name = jsp_res.getString("data.last_name" + "[" + i + "]");

			String Exp_avatar = exp_avatar_array[i];
			String res_avatar = jsp_res.getString("data.avatar" + "[" + i + "]");

			Assert.assertEquals(res_id, Exp_id);
			Assert.assertEquals(res_email, Exp_email);
			Assert.assertEquals(res_first_name, Exp_first_name);
			Assert.assertEquals(res_last_name, Exp_last_name);
			Assert.assertEquals(res_avatar, Exp_avatar);

		}

	}

}
