package testclasspackage;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import commonmethodpackage.Trigger_API_PostMethod;
import commonutilitypackage.Handle_logs;
import io.restassured.path.json.JsonPath;

public class Post_TC1 extends Trigger_API_PostMethod {
@Test
	public static void executor() throws IOException{
		File DirectoryName = Handle_logs.Create_log_Directory("Post_TC1");
		String requestbody = post_tc1_request();
		for (int i = 0; i < 5; i++) {

			int status_code = Trigger_API_PostMethod.extract_status_code(requestbody, post_endpoint());
			System.out.println(status_code);
			if (status_code == 201) {

				String responseBody = extract_responsebody(requestbody, post_endpoint());
				System.out.println(responseBody);
				Handle_logs.evidence_creator(DirectoryName, "Post_TC1", post_endpoint(),post_tc1_request(), responseBody);
				validator(requestbody , responseBody);
				break;
			} else {
				System.out.println("Desired status code not found hence try again");
			}
		}
	}

	public static void validator(String requestbody, String responseBody) {
		JsonPath jsp_res = new JsonPath(responseBody);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");

		JsonPath jsp_req = new JsonPath(requestbody);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
	}
}
