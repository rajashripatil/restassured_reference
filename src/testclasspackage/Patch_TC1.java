package testclasspackage;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import commonmethodpackage.Trigger_API_PatchMethod;
import commonutilitypackage.Handle_logs;
import io.restassured.path.json.JsonPath;

public class Patch_TC1 extends Trigger_API_PatchMethod {
	@Test
	public static void executor() throws IOException {
		File DirectoryName= Handle_logs.Create_log_Directory("Patch_TC1");
		String requestBody = patch_tc1_request();
		for (int i = 0; i < 5; i++) {

			int status_code = Trigger_API_PatchMethod.extract_status_code(patch_tc1_request(), patch_endpoint());
			System.out.println(status_code);
			if (status_code == 200) {

				String responseBody = extract_responsebody(patch_tc1_request(), patch_endpoint());
				System.out.println(responseBody);
				Handle_logs.evidence_creator(DirectoryName,"Patch_TC1", patch_tc1_request(), patch_endpoint(), responseBody);
				validator(requestBody , responseBody);
				break;
			} else {
				System.out.println("Desired status code not found hence try again");
			}
		}
	}

	public static void validator(String requestBody ,String responseBody) {
		JsonPath jsp_res = new JsonPath(responseBody);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");

		JsonPath jsp_req = new JsonPath(requestBody);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
	}

}
